from django.shortcuts import render, get_object_or_404
from . import models


def base(request):
    base_list = models.logo.objects.all()
    base_list1 = models.drbare_ma.objects.all()
    return render(request, 'base.html', {'base_list': base_list,
                                         'base_list1': base_list1})


def index(request):
    index_listo = models.hspio.objects.all()[:3]
    product_list = models.hspia.objects.all()
    base_list = models.logo.objects.all()
    base_list1 = models.drbare_ma.objects.all()
    return render(request, 'index.html', {'index_listo': index_listo,
                                          'product_list': product_list,
                                          'base_list': base_list,
                                          'base_list1': base_list1})


def store(request):
    store_list = models.hspia.objects.all()
    base_list = models.logo.objects.all()
    base_list1 = models.drbare_ma.objects.all()
    return render(request, 'store.html', {'store_list': store_list,
                                          'base_list': base_list,
                                          'base_list1': base_list1})


def product(request, pk):
    base_list = models.logo.objects.all()
    base_list1 = models.drbare_ma.objects.all()
    product_d = get_object_or_404(models.hspia, id=pk)
    return render(request, 'product.html', {'product_d': product_d,
                                            'base_list': base_list,
                                            'base_list1': base_list1})
