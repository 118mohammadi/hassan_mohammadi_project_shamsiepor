from audioop import reverse
from datetime import datetime
from django.contrib.auth.models import User
from django.db import models
from django.urls import reverse


class logo(models.Model):
    name = models.CharField(max_length=50)
    create_time: datetime = models.DateTimeField(auto_now_add=True)
    update_time = models.DateTimeField(auto_now=True)
    image = models.ImageField(upload_to='images/base/%Y/%m/%d', blank=True)

    class Meta:
        ordering = ('create_time',)

    def __str__(self):
        return self.name


class drbare_ma(models.Model):
    name = models.CharField(max_length=50)
    namber = models.CharField(max_length=12)
    n_email = models.CharField(max_length=100)
    n_insta = models.CharField(max_length=100)
    addres = models.CharField(max_length=100)
    create_time = models.DateTimeField(auto_now_add=True)
    update_time = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('create_time',)

    def __str__(self):
        return self.name


class hspio(models.Model):
    name = models.CharField(max_length=50)
    n_h2 = models.CharField(max_length=50)
    n_span = models.CharField(max_length=100)
    n_p = models.CharField(max_length=300)
    create_time = models.DateTimeField(auto_now_add=True)
    update_time = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('create_time',)

    def __str__(self):
        return self.name


class hspia(models.Model):
    name = models.CharField(max_length=50)
    n_h2 = models.CharField(max_length=50)
    n_span = models.CharField(max_length=100)
    n_p = models.CharField(max_length=1000)
    image = models.ImageField(upload_to='images/index/%Y/%m/%d', blank=True)
    create_time = models.DateTimeField(auto_now_add=True)
    update_time = models.DateTimeField(auto_now=True)
    price = models.DecimalField(max_digits=10, decimal_places=0, default=None)


    class Meta:
        ordering = ('create_time',)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('shop:product', args=[self.id])
